#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
use serde::Serialize;
use rocket_contrib::json::Json;

#[derive(Serialize)]
struct PaymentResult {
    success: bool,
    message: String
}

#[post("/charge?<price>&<balance>")]
fn charge(price: i64, balance: i64) -> Json<PaymentResult> {
    return if price > balance {
        Json(PaymentResult {
            success: false,
            message: "Price is greater than balance".to_string()
        })
    } else {
        Json(PaymentResult {
            success: true,
            message: "Charged successfully".to_string()
        })
    }
}

fn main() {
    rocket::ignite().mount("/payment-gateway", routes![charge]).launch();
}
