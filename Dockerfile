# Build Stage
FROM rust:1.52.1 AS builder
WORKDIR /usr/src/
RUN apt update && apt-get install -y musl-tools && rm -rf /var/lib/apt/lists/*
RUN rustup default nightly
RUN rustup target add x86_64-unknown-linux-musl

RUN USER=root cargo new payment_gateway
WORKDIR /usr/src/payment_gateway
COPY Cargo.toml Cargo.lock ./
RUN cargo build --release

COPY src ./src
RUN cargo install --target x86_64-unknown-linux-musl --path .

# Bundle Stage
FROM scratch
COPY --from=builder /usr/local/cargo/bin/payment_gateway .
USER 1000
CMD ["./payment_gateway", "-a", "0.0.0.0", "-p", "8092"]
